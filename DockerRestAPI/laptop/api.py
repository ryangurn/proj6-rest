# Laptop Service
import datetime
import os

import arrow
import pymongo
from flask import Flask, redirect, url_for, request, render_template, session, jsonify
from pymongo import MongoClient
import acp_times  # Brevet time calculations
import config  # config
import logging
from flask import Flask
from flask_restful import Resource, Api

# Instantiate the app
app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
api = Api(app)

# DB CONNECTION
# client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
# client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 34542)
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevet


# Index
@app.route("/")
@app.route("/index")
def index():
    count = db.data.find({}).count()
    g = True

    if count <= 0:
        g = False

    app.logger.debug("Main page entry")
    return render_template('calc.html', get=g)


# Error Handlers
@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    session['linkback'] = url_for("index")
    return render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('dist', 200, type=int)
    start = request.args.get('start', None, type=str)
    app.logger.debug("km={}".format(km))
    app.logger.debug("dist={}".format(dist))
    app.logger.debug("start={}".format(start))
    app.logger.debug("request.args: {}".format(request.args))

    a = arrow.get(start, 'YYYY-MM-DD HH:mm', tzinfo='US/Pacific')

    open_time = acp_times.open_time(km, dist, a.isoformat())
    close_time = acp_times.close_time(km, dist, a.isoformat())
    result = {"open": open_time, "close": close_time}
    return jsonify(result=result)


@app.route('/submit', methods=['POST'])
def submit():
    """
    Submits the data from the form to the mongodb database
    this will ensure that the data is stored for future use
    for the API and act as a set of digest logic for the db.
    :return: None
    """
    data = request.form
    iterator = 0
    dist = data.get('distance')
    begin_date = data.get('begin_date')
    begin_time = data.get('begin_time')

    # organize the data into an array
    miles = []
    km = []
    open = []
    close = []
    length = 0
    for v in data:
        split = v.split("_", 1)
        if split[0] != 'begin':  # strip out begin time and begin date
            if len(split) == 2:  # remove distance
                key = split[1]  # index
                value = split[0]  # miles/km/open/close
                if data["_".join(split)] != "":  # remove empty input slots
                    if value == "miles":
                        miles.append(data["_".join(split)])
                        length += 1 / 4
                    elif value == "km":
                        km.append(data["_".join(split)])
                        length += 1 / 4
                    elif value == "close":
                        close.append(data["_".join(split)])
                        length += 1 / 4
                    elif value == "open":
                        open.append(data["_".join(split)])
                        length += 1 / 4
                    print(length, key, value, "_".join(split), data["_".join(split)])
    length = int(length)

    if length == 0:
        return redirect('/')

    output = {}
    for i in range(0, length):
        output.update({str(i): {"miles": miles[i], "km": km[i], "open": open[i], "close": close[i]}})

    db.data.insert_one(
        {'data': output, 'created': datetime.datetime.utcnow(), 'distance': dist, 'begin_date': begin_date,
         'begin_time': begin_time})
    return redirect('/')


@app.route('/get')
def get():
    """
    Allow the user to get a view of the data that is currently being
    stored within mongo for use by the API. This will return a view
    to the user of the most recent set of brevet calculations.
    :return:
    """
    data = db.data.find().sort('created', pymongo.DESCENDING).limit(1)
    newData = None
    for d in data:
        newData = d

    viewData = newData['data']
    distance = newData['distance']

    return render_template('get.html', distance=distance, data=viewData)


def __listAll(removeT=None, t='json', top=None):
    data = db.data.find({})

    if t is 'json':
        newData = []
        for d in data:
            obj = {
                'id': str(d['_id'])
            }
            count = 0
            for a in d['data']:
                if removeT is not None:
                    del d['data'][a][removeT]
                if top is None:
                    obj.update({a: d['data'][a]})
                elif count <= (int(top)-1):
                    obj.update({a: d['data'][a]})
                    count += 1
            newData.append(obj)
        return jsonify(newData)
    elif t is 'csv':
        if removeT is 'open':
            retStr = 'id,miles,km,close,\n'
        elif removeT is 'close':
            retStr = 'id,miles,km,open,\n'
        elif removeT is None:
            retStr = 'id,miles,km,open,close,\n'
        for d in data:
            for a in d['data']:
                if removeT is not None:
                    del d['data'][a][removeT]
                retStr += str(d['_id']) + ","
                for dd in d['data'][a]:
                    retStr += str(d['data'][a][dd]) + ","
                retStr += "\n"
        return retStr


@app.route('/listAll')
def listAll():
    top = request.args.get('top')
    return __listAll(top=top)


@app.route('/listAll/json')
def listAllJSON():
    top = request.args.get('top')
    return __listAll(top=top)


@app.route('/listAll/csv')
def listAllCSV():
    top = request.args.get('top')
    return __listAll(t='csv', top=top)


@app.route('/listOpenOnly')
def listOpenOnly():
    top = request.args.get('top')
    return __listAll('close', 'json', top)


@app.route('/listOpenOnly/json')
def listOpenOnlyJSON():
    top = request.args.get('top')
    return __listAll('close', 'json', top)


@app.route('/listOpenOnly/csv')
def listOpenOnlyCSV():
    top = request.args.get('top')
    return __listAll('close', 'csv', top)


@app.route('/listCloseOnly')
def listCloseOnly():
    top = request.args.get('top')
    return __listAll('open', 'json', top)


@app.route('/listCloseOnly/json')
def listCloseOnlyJSON():
    top = request.args.get('top')
    return __listAll('open', 'json', top)


@app.route('/listCloseOnly/csv')
def listCloseOnlyCSV():
    top = request.args.get('top')
    return __listAll('open', 'csv', top)


app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

# Run the application
if __name__ == '__main__':
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(host='0.0.0.0', port=CONFIG.PORT, debug=CONFIG.DEBUG)
