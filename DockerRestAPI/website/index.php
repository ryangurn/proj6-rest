<html>
    <head>
        <title>CIS 322 REST-api demo: Brevet List</title>
    </head>

    <body>
        <h1>List of Brevets & Associated Data</h1>
        <ul>
            <?php
            $json = file_get_contents('http://laptop-service/listAll');
            $obj = json_decode($json, true);
	        foreach($obj as $k => $o){
	            foreach($o as $k1 => $o1){
	                if(isset($o['id'], $o1['open'], $o1['close'])){
                        echo "<li><u>". $o['id'] . '</u> <b>#'. $k1. '</b> ';
                        echo "([O]:".$o1['open']." - [C]:".$o1['close'].")";
                        echo "</li>";
                    }
	            }
	        }
            ?>
        </ul>
    </body>
</html>
